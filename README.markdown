Student Registration
====================

[![Build Status](https://travis-ci.org/agudaorgil/studreg.svg?branch=master)](https://travis-ci.org/agudaorgil/studreg)
[![Coverage Status](https://coveralls.io/repos/agudaorgil/studreg/badge.svg)](https://coveralls.io/r/agudaorgil/studreg)
[![Code Health](https://landscape.io/github/agudaorgil/studreg/master/landscape.svg?style=flat)](https://landscape.io/github/agudaorgil/studreg/master)

http://aguda.org.il's student registration and management system.
